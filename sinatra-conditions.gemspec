Gem::Specification.new do |s|
	s.name			= 'sinatra-conditions'
	s.version		= '0.0.1'
	s.date			= Date.today.to_s
	s.summary		= "Sinatra Extension for additional conditions"
	s.description	= "Sinatra Conditions gives you the ability to use additional conditions."
	s.authors		= ["Alexander Popov"]
	s.email			= "alex.wayfer@gmail.com"
	s.files	= [
		"lib/sinatra/conditions.rb"
	]
	s.homepage		= "https://bitbucket.org/AlexWayfer/sinatra-conditions"
	s.license		= "MIT"

	s.add_runtime_dependency 'sinatra', '~> 1'
end
