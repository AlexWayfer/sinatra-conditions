require 'sinatra/base'

module Sinatra
	module Conditions

		def self.included(app)
			app.register self
		end

		def self.registered(app)

			## For method condition
			## Example: `before '/', :method => :post do`
			app.set(:method) do |method|
				method = method.to_s.upcase
				condition { request.request_method == method }
			end

		end

	end

	register Conditions
end